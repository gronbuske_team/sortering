#pragma once

struct Node
{
	Node* next;
	Node* previous;
	int content;
};

class DLList
{
public:
	DLList(int seed);
	~DLList();

	void Populate(int n, int max);
	void InsertionSort();
	void QuickSort();
	void Print();

private:
	void InsertionSort(Node* l, Node* r);
	void QuickSortRec(Node* l, Node* r);
	Node* Partition(Node* l, Node* r);
	void Swap(int* a, int* b);

	Node* root;
};


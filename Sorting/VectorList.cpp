#include "VectorList.h"
#include <stdlib.h>
#include <random>
#include <time.h>
#include <iostream>


VectorList::VectorList(int seed)
{
	//std::srand(time(nullptr));
	std::srand(seed);
}


VectorList::~VectorList()
{
	free(list);
}

void VectorList::InitiateList(int size)
{
	list = (int*)malloc(sizeof(int)* size);
	this->size = size;
}

void VectorList::Populate(int n, int max)
{
	for (int i = 0; i < n; i++)
	{
		list[i] = std::rand() % max;
	}
}

void VectorList::InsertionSort()
{
	int temp;
	for (int i = 0; i < size - 1; i++)
	{
		int j = i + 1;
		while (list[j] < list[j - 1] && j >= 1)
		{
			temp = list[j];
			list[j] = list[j - 1];
			list[j - 1] = temp;
			j--;
		}
	}
}

void VectorList::InsertionSort(int l, int r)
{
	int temp;
	for (int i = l; i <= r; i++)
	{
		int j = i + 1;
		while (list[j] < list[j - 1] && j >= 1)
		{
			temp = list[j];
			list[j] = list[j - 1];
			list[j - 1] = temp;
			j--;
		}
	}
}

void VectorList::QuickSort()
{
	QuickSortRec(0, size - 1);
}

void VectorList::Print()
{
	for (int i = 0; i < size; i++)
	{
		std::cout << list[i] << " ";
	}
	std::cout << std::endl;
}

void VectorList::QuickSortRec(int l, int r)
{
	//*/
	if (r - l <= 70)
	{
		InsertionSort(l, r);
		return;
	}
	//*/

	int i = l;
	int j = r;
	int temp;
	int pivot = list[(l + r) / 2];

	while (i <= j)
	{
		while (list[i] < pivot)
			i++;
		while (list[j] > pivot)
			j--;
		if (i <= j)
		{
			temp = list[i];
			list[i] = list[j];
			list[j] = temp;
			i++;
			j--;
		}
	}

	if (l < j)
		QuickSortRec(l, j);
	if (i < r)
		QuickSortRec(i, r);
}

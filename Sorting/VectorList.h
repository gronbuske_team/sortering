#pragma once

class VectorList
{
public:
	VectorList(int seed);
	~VectorList();

	void InitiateList(int size);

	void Populate(int n, int max);
	void InsertionSort();
	void QuickSort();
	void Print();

	int* list;
	int size;

private:
	void QuickSortRec(int l, int r);
	void InsertionSort(int l, int r);
};


#include "DLList.h"
#include "VectorList.h"
#include <iostream>
#include <ctime>

void main()
{
	const int SEED = 70;

	clock_t commence, complete;
	commence = clock();

	complete = clock();
	long lTime = (complete - commence) * 1000 / CLOCKS_PER_SEC;


	DLList dllist = DLList(SEED);
	dllist.Populate(1000000, 500000);
	commence = clock();
	//dllist.InsertionSort();
	dllist.QuickSort();
	complete = clock();
	long dlTime = (complete - commence) * 1000 / CLOCKS_PER_SEC;
	//dllist.Print();

	VectorList vList = VectorList(SEED);
	vList.InitiateList(1000000);
	vList.Populate(1000000, 5000);
	commence = clock();
	//vList.InsertionSort();
	vList.QuickSort();
	complete = clock();
	long vTime = (complete - commence) * 1000 / CLOCKS_PER_SEC;
	//vList.Print();

	std::cout << std::endl << std::endl << dlTime << ":Double Linked List";
	std::cout << std::endl << vTime << ":Vector List";
	/*
	Vektorlistan �r lite snabbare �r den dubbell�nkade listan n�r det kommer till 
	insertionsorten. Detta kan bero p� att minnes�tkomsten �r lite snabbare med 
	vektorn p� grund av att datan i en vektor ligger intill varandra i r�tt ordning.

	�ven vid ren quicksort �r vektorlistan lite snabbare, vid testning av 1 000 000 heltal 
	fick vi resultaten Vektorsort: ~82 ms och Dubbell�nkad: ~220 ms
	Efter lite inst�llningar med n�r Insertionsort ska anv�ndas fick vi ner det till 
	Vektor: ~76 ms och Dubbell�nkad: ~170 ms.
	Insertionsort fungerade b�ttre vid ca 50-90 objekt f�r vektorlistan och 
	vid 40-60 f�r den dubbell�nkade.
	*/



	char input = ' ';
	while (input != 'q')
	{
		std::cin >> input;
	}
}
#include "DLList.h"
#include <random>
#include <time.h>
#include <iostream>


DLList::DLList(int seed)
{
	//std::srand(time(nullptr));
	std::srand(seed);
}


DLList::~DLList()
{
}

void DLList::QuickSortRec(Node* l, Node* r)
{
	if (r != nullptr && l != r && l != r->next)
	{
		Node *p = Partition(l, r);
		///*
		if (p == nullptr)
		{
			InsertionSort(l, r);
			return;
		}
		//*/

		QuickSortRec(l, p->previous);
		QuickSortRec(p->next, r);
	}
}

Node* DLList::Partition(Node* l, Node* r)
{
	int pivot = r->content;
	Node* i = l->previous;

	int counter = 0;
	for (Node* j = l; j != r; j = j->next)
	{
		counter++;
		if (j->content <= pivot)
		{
			i = (i == nullptr) ? l : i->next;

			Swap(&i->content, &j->content);
		}
	}
	if (counter < 50)
		return nullptr;
	i = (i == nullptr) ? l : i->next;
	Swap(&i->content, &r->content);
	return i;
}

void DLList::Swap(int* a, int* b)
{
	int t = *a;      *a = *b;       *b = t;
}

void DLList::Populate(int n, int max)
{
	Node* activeNode = new Node();
	Node* temp;
	root = activeNode;
	for (int i = 0; i < n; i++)
	{
		activeNode->content = std::rand() % max;
		temp = new Node();
		activeNode->next = temp;
		temp->previous = activeNode;
		activeNode = temp;
	}
	activeNode->content = std::rand() % max;
}

void DLList::InsertionSort()
{
	Node* activeNode = root;
	Node* temp;
	while (activeNode != nullptr && activeNode->next != nullptr)
	{
		temp = activeNode->next;
		while (temp->previous != nullptr)
		{
			if (temp->content < temp->previous->content)
			{
				Swap(&temp->content, &temp->previous->content);
				temp = temp->previous;
			} 
			else
			{
				break;
			}
		}
		activeNode = activeNode->next;
	}
}

void DLList::InsertionSort(Node* l, Node* r)
{
	Node* activeNode = l;
	Node* temp;
	while (activeNode != nullptr && activeNode != r)
	{
		temp = activeNode->next;
		while (temp->previous != nullptr)
		{
			if (temp->content < temp->previous->content)
			{
				Swap(&temp->content, &temp->previous->content);
				temp = temp->previous;
			}
			else
			{
				break;
			}
		}
		activeNode = activeNode->next;
	}
}

void DLList::QuickSort()
{
	Node* r = root;
	while (r->next != nullptr)
		r = r->next;
	QuickSortRec(root, r);
}

void DLList::Print()
{
	Node* activeNode = root;
	while (activeNode != nullptr)
	{
		std::cout << activeNode->content << " ";
		activeNode = activeNode->next;
	}
	std::cout << std::endl;
}
